<?php

namespace App\Facades\Company;

use Illuminate\Support\Facades\Facade;

class CompanyFilterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'company_filter';
    }
}