<?php

namespace App\Facades\Department;

use Illuminate\Support\Facades\Facade;

class DepartmentFilterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'department_filter';
    }
}