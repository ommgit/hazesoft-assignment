<?php

namespace App\Facades\Employee;

use Illuminate\Support\Facades\Facade;

class EmployeeFilterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'employee_filter';
    }
}