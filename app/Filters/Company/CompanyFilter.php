<?php

namespace App\Filters\Company;

use App\Models\Company;

class CompanyFilter
{
    public function apply(Company $company, array $filterParams)
    {
        $company = ($company)->newQuery();
        $sorting = !in_array($filterParams['sorting'], ['asc', 'desc']) ? 'desc' : $filterParams['sorting'];
        $company->when(isset($filterParams['name']), function ($query) use ($filterParams) {
            $name = $filterParams['name'];
            return $query->where('name', 'like', "%$name%");
        })->when(isset($filterParams['location']), function ($query) use ($filterParams) {
            $location = $filterParams['location'];
            return $query->where('location', 'like', "%$location%");
        })->when(isset($filterParams['contact']), function ($query) use ($filterParams) {
            $contact = $filterParams['contact'];
            return $query->where('contact', 'like', "%$contact%");
        });
        $company->orderBy('id', $sorting);

        return isset($filterParams['records_per_page'])
            ? $company->paginate($filterParams['records_per_page'])
            : $company->get();
    }
}
