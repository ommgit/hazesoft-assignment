<?php

namespace App\Filters\Department;

use App\Models\Department;

class DepartmentFilter
{
    public function apply(Department $department, array $filterParams)
    {
        $department = ($department)->newQuery();
        $sorting = !in_array($filterParams['sorting'], ['asc', 'desc']) ? 'desc' : $filterParams['sorting'];
        $department->with('company:id,name')
            ->when(isset($filterParams['name']), function ($query) use ($filterParams) {
                $name = $filterParams['name'];
                return $query->where('name', 'like', "%$name%");
            })->when(isset($filterParams['company']), function ($query) use ($filterParams) {
                $companySearchParam = $filterParams['company'];
                return $query->whereHas('company', function ($query) use ($companySearchParam) {
                    return $query->where('name', 'like', "%$companySearchParam%");
                });
            })->when(isset($filterParams['company_id']), function ($query) use ($filterParams) {
                $companyId = $filterParams['company_id'];
                return $query->where('company_id', $companyId);
            });
        $department->orderBy('id',$sorting);

        return isset($filterParams['records_per_page'])
            ? $department->paginate($filterParams['records_per_page'])
            : $department->get();
    }

}
