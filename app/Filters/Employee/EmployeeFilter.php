<?php

namespace App\Filters\Employee;

use App\Models\Employee;

class EmployeeFilter
{
    public function apply(Employee $employee, array $filterParams)
    {
        $employee = ($employee)->newQuery();
        $sorting = !in_array($filterParams['sorting'], ['asc', 'desc']) ? 'desc' : $filterParams['sorting'];
        $employee->with(['company:id,name', 'departments:id,name'])
            ->when(isset($filterParams['employee_number']), function ($query) use ($filterParams) {
                $employee_number = $filterParams['employee_number'];
                return $query->where('employee_number', 'like', "%$employee_number%");
            })->when(isset($filterParams['name']), function ($query) use ($filterParams) {
                $name = $filterParams['name'];
                return $query->where('name', 'like', "%$name%");
            })->when(isset($filterParams['email']), function ($query) use ($filterParams) {
                $email = $filterParams['email'];
                return $query->where('email', 'like', "%$email%");
            })->when(isset($filterParams['contact']), function ($query) use ($filterParams) {
                $contact = $filterParams['contact'];
                return $query->where('contact', 'like', "%$contact%");
            })->when(isset($filterParams['designation']), function ($query) use ($filterParams) {
                $designation = $filterParams['designation'];
                return $query->where('designation', 'like', "%$designation%");
            })->when(isset($filterParams['company']), function ($query) use ($filterParams) {
                $companySearchParam = $filterParams['company'];
                return $query->whereHas('company', function ($query) use ($companySearchParam) {
                    return $query->where('name', 'like', "%$companySearchParam%");
                });
            })->when(isset($filterParams['company_id']), function ($query) use ($filterParams) {
                $companyId = $filterParams['company_id'];
                return $query->where('company_id', $companyId);
            })->when(
                isset($filterParams['department_ids']) && count(array_filter($filterParams['department_ids'])) > 0,
                function ($query) use ($filterParams) {
                    $departmentIds = $filterParams['department_ids'];
                    return $query->whereHas('departments', function ($query) use ($departmentIds) {
                        return $query->whereIn('department_id', $departmentIds);
                    });
                }
            );

        $employee->orderBy('id', $sorting);

        return isset($filterParams['records_per_page'])
            ? $employee->paginate($filterParams['records_per_page'])
            : $employee->get();
    }

}
