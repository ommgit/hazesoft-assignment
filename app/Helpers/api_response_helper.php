<?php

if (!function_exists('respondSuccess')) {
    function respondSuccess($message, $data = null)
    {
        $response = [
            'error' => false,
            'message' => $message,
            'code' => 200
        ];

        if (!is_null($data)) {
            $response['data'] = $data;
        }
        return response()->json($response, 200);
    }
}

if (!function_exists('respondError')) {
    function respondError($message, $code = 500, array $errorFields = [])
    {
        $response = [
            'error' => true,
            'message' => $message,
            'code' => $code,
        ];

        if (count($errorFields) > 0) {
            $response['data'] = $errorFields;
        }
        if ($code < 100 || !is_numeric($code) || $code > 599) {
            $code = 500;
            $response['code'] = $code;
        }
        return response()->json($response, $code);
    }
}