<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CompanyCreateRequest;
use App\Http\Resources\Company\CompanyCollection;
use App\Http\Resources\Company\CompanyResource;
use App\Services\Company\CompanyService;
use Exception;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    public function index(Request $request)
    {
        $companies = $this->companyService->getCompanies($request);
        return (new CompanyCollection($companies))->preserveQuery();
    }

    public function store(CompanyCreateRequest $request)
    {
        try {
            $validatedRequests = $request->validated();
            $company = $this->companyService->saveCompany($validatedRequests);
            $company = new CompanyResource($company);
            return respondSuccess('Company Saved Successfully', $company);
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

}
