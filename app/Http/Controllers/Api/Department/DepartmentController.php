<?php

namespace App\Http\Controllers\Api\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\Department\DepartmentCreateRequest;
use App\Http\Resources\Department\DepartmentCollection;
use App\Http\Resources\Department\DepartmentResource;
use App\Services\Department\DepartmentService;
use Exception;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    protected $departmentService;

    public function __construct(DepartmentService $departmentService)
    {
        $this->departmentService = $departmentService;
    }

    public function index(Request $request)
    {
        try {
            $departments = $this->departmentService->listOfDepartments($request);
            return (new DepartmentCollection($departments))->preserveQuery();
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

    public function store(DepartmentCreateRequest $request)
    {
        try {
            $validatedRequests = $request->validated();
            $department = $this->departmentService->saveDepartment($validatedRequests);
            $department = new DepartmentResource($department);
            return respondSuccess('Department Saved Successfully', $department);
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

    public function getDepartmentsOfCompany(Request $request, $companyId)
    {
        try {
            $departments = $this->departmentService->getDepartmentsOfCompany($request, $companyId);
            return (new DepartmentCollection($departments))->preserveQuery();
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

}
