<?php

namespace App\Http\Controllers\Api\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\EmployeeCreateRequest;
use App\Http\Resources\Employee\EmployeeCollection;
use App\Http\Resources\Employee\EmployeeResource;
use App\Services\Employee\EmployeeService;
use Exception;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    protected $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function index(Request $request)
    {
        try {
            $employees = $this->employeeService->listOfEmployees($request);
            return (new EmployeeCollection($employees))->preserveQuery();
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

    public function store(EmployeeCreateRequest $request)
    {
        try {
            $validatedRequests = $request->validated();
            $employee = $this->employeeService->saveEmployee($validatedRequests);
            $employee = new EmployeeResource($employee);
            return respondSuccess('Employee Saved Successfully', $employee);
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

    public function getEmployeesOfCompany(Request $request, $companyId)
    {
        try {
            $employees = $this->employeeService->getEmployeesOfCompany($request, $companyId);
            return (new EmployeeCollection($employees))->preserveQuery();
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

    public function getEmployeesOfDepartmentOfCompany(Request $request, $companyId, $departmentId)
    {
        try {
            $employees = $this->employeeService->getEmployeesOfDepartmentOfCompany($request, $companyId, $departmentId);
            return (new EmployeeCollection($employees))->preserveQuery();
        } catch (Exception $e) {
            return respondError('Something Went Wrong !', $e->getCode());
        }
    }

    public function showEmployeeDetails($employeeId)
    {
        try {
            $employee = $this->employeeService->showEmployeeDetails($employeeId);
            $employee = new EmployeeResource($employee);
            return respondSuccess('Employee Details Found', $employee);
        } catch (Exception $e) {
            return respondError($e->getMessage(), $e->getCode());
        }
    }

}
