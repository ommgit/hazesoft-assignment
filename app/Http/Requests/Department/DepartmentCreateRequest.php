<?php

namespace App\Http\Requests\Department;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DepartmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'company_id' => ['bail', 'required', 'exists:companies,id'],
            'name' => [
                'bail',
                'required',
                'max:191',
                Rule::unique('departments')->where(function ($query) {
                    return $query->where('company_id', $this->get('company_id'));
                }),
            ],

        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'Same department has existed in the company',
            'company_id.required' => 'Company is required',
            'company_id.exists' => 'Invalid company '
        ];
    }
}
