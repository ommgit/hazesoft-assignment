<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required', 'max:191'],
            'email' => ['bail', 'required', 'unique:employees,email', 'max:191'],
            'contact' => ['bail', 'required', 'max:20', 'unique:employees,contact'],
            'designation' => ['bail', 'required', 'max:191'],
            'company_id' => ['bail', 'required', 'exists:companies,id'],
            'department_ids' => ['bail', 'required_with:company_id', 'array', 'min:1'],
            'department_ids.*' => [
                'required',
                Rule::exists('departments','id')->where(function ($query) {
                    return $query->where('company_id', $this->get('company_id'));
                }),
            ]
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'This employee email has already been taken',
            'company_id.exists' => 'Invalid Company',
            'department_ids.*.required' => 'Please input department at #:position.',
            'department_ids.*.exists' => 'Department in #:position. is invalid',
        ];
    }
}
