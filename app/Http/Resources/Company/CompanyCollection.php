<?php

namespace App\Http\Resources\Company;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use JsonSerializable;

class CompanyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return CompanyResource::collection($this->collection);
    }

    public function with($request)
    {
        return [
            'message' => 'Results Found',
            'error' => false,
            'code' => 200
        ];
    }
}
