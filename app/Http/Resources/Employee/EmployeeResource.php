<?php

namespace App\Http\Resources\Employee;

use App\Http\Resources\Department\DepartmentCollection;
use App\Http\Resources\Department\DepartmentResource;
use App\Http\Resources\Department\MinimalDepartmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'employee_number' => $this->employee_number,
            'email' => $this->email,
            'contact' => $this->contact,
            'designation' => $this->designation,
            'company' => $this->company->name,
            'departments' => MinimalDepartmentResource::collection($this->departments)
        ];
    }
}
