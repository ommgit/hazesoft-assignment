<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $table = "departments";
    protected $fillable = [
        'name',
        'company_id',
        'created_by'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (auth()->check()) {
                $model->created_by = auth()->id();
            }
        });
    }

    public function employees()
    {
        return $this->belongsToMany(
            Employee::class,
            'department_employee',
            'department_id',
            'employee_id'
        );
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
