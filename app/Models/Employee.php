<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = "employees";
    protected $fillable = [
        'name',
        'employee_number',
        'contact',
        'email',
        'designation',
        'company_id',
        'created_by'
    ];


    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->employee_number = $model->generateEmployeeNumber();
            if (auth()->check()) {
                $model->created_by = auth()->id();
            }
        });
    }

    public function generateEmployeeNumber()
    {
        $prefix = 'EMP';
        $initialIndex = 1;
        $employee = $this->latest('id')->first();
        if ($employee) {
            $newNumber = (int)(str_replace($prefix, "", $employee->employee_number) + 1);
            $newEmployeeNumber = $prefix . $newNumber;
        } else {
            $newEmployeeNumber = $prefix . $initialIndex;
        }
        return $newEmployeeNumber;
    }

    /**
     * The company that belong to the employee.
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    /**
     * The departments that belong to the employee.
     */
    public function departments()
    {
        return $this->belongsToMany
        (
            Department::class,
            'department_employee',
            'employee_id',
            'department_id'
        );
    }

}
