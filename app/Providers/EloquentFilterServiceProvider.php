<?php

namespace App\Providers;

use App\Filters\Company\CompanyFilter;
use App\Filters\Department\DepartmentFilter;
use App\Filters\Employee\EmployeeFilter;
use Illuminate\Support\ServiceProvider;

class EloquentFilterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('company_filter', function () {
            return new CompanyFilter();
        });

        $this->app->bind('department_filter', function () {
            return new DepartmentFilter();
        });

        $this->app->bind('employee_filter', function () {
            return new EmployeeFilter();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
