<?php

namespace App\Repositories\Company;

use App\Models\Company;
use CompanyFilter;

class CompanyRepository
{
    protected $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function getCompanies($filterParams)
    {
        return CompanyFilter::apply($this->company, $filterParams);
    }

    public function saveCompany($validatedRequests)
    {
        return $this->company->create($validatedRequests)->fresh();
    }

}