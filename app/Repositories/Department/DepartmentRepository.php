<?php

namespace App\Repositories\Department;

use App\Models\Department;
use DepartmentFilter;

class DepartmentRepository
{
    protected $department;

    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    public function getDepartments($filterParams)
    {
        return DepartmentFilter::apply($this->department, $filterParams);
    }

    public function saveDepartment($validatedRequests)
    {
        return $this->department->create($validatedRequests)->fresh();
    }

}