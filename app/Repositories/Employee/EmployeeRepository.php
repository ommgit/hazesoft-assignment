<?php

namespace App\Repositories\Employee;

use App\Models\Employee;
use EmployeeFilter;

class EmployeeRepository
{
    protected $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function getEmployees(array $filterParams)
    {
        return EmployeeFilter::apply($this->employee, $filterParams);
    }

    public function saveEmployee($validatedRequests)
    {
        return $this->employee->create($validatedRequests)->fresh();
    }

    public function syncEmployeeInDepartments($employee, array $departmentIds)
    {
        return $employee->departments()->attach($departmentIds);
    }

    public function showEmployeeDetails($employeeId)
    {
        return $this->employee->where('id', $employeeId)
            ->with('company:id,name', 'departments:id,name')
            ->first();
    }

}
