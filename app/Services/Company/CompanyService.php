<?php

namespace App\Services\Company;

use App\Repositories\Company\CompanyRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyService
{
    protected $companyRepo;

    public function __construct(CompanyRepository $companyRepo)
    {
        $this->companyRepo = $companyRepo;
    }

    public function getCompanies(Request $request)
    {
        $filterParams = [
            'name' => $request->get('name'),
            'location' => $request->get('location'),
            'contact' => $request->get('contact'),
            'records_per_page' => $request->has('records_per_page')
                ? $request->get('records_per_page') : 10,
            'sorting' => $request->has('sorting')
                ? $request->get('sorting') : 'desc'
        ];
        return $this->companyRepo->getCompanies($filterParams);
    }

    public function saveCompany($validatedRequests)
    {
        try {
            DB::beginTransaction();
            $company = $this->companyRepo->saveCompany($validatedRequests);
            DB::commit();
            return $company;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}