<?php

namespace App\Services\Department;

use App\Repositories\Department\DepartmentRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentService
{
    protected $departmentRepo;

    public function __construct(DepartmentRepository $departmentRepo)
    {
        $this->departmentRepo = $departmentRepo;
    }

    public function listOfDepartments(Request $request)
    {
        $filterParams = [
            'company' => $request->get('company'),
        ];
        $filterParams = $this->searchables($request) + $filterParams;
        return $this->departmentRepo->getDepartments($filterParams);
    }

    public function searchables(Request $request): array
    {
        return [
            'name' => $request->get('name'),
            'records_per_page' => $request->has('records_per_page')
                ? $request->get('records_per_page') : 10,
            'sorting' => $request->has('sorting')
                ? $request->get('sorting') : 'desc'
        ];
    }

    public function saveDepartment($validatedRequests)
    {
        try {
            DB::beginTransaction();
            $department = $this->departmentRepo->saveDepartment($validatedRequests);
            DB::commit();
            return $department;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getDepartmentsOfCompany(Request $request, $companyId)
    {
        try {
            $filterParams = [
                'company_id' => $companyId,
            ];
            $filterParams = $this->searchables($request) + $filterParams;
            $departments = $this->departmentRepo->getDepartments($filterParams);
            return $departments;
        } catch (Exception $e) {
            throw $e;
        }
    }

}