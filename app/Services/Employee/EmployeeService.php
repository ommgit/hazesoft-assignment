<?php

namespace App\Services\Employee;

use App\Repositories\Employee\EmployeeRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeService
{
    protected $employeeRepo;

    public function __construct(EmployeeRepository $employeeRepo)
    {
        $this->employeeRepo = $employeeRepo;
    }

    public function listOfEmployees(Request $request)
    {
        $filterParams = [
            'company' => $request->get('company'),
            'department_ids' => $request->get('department_ids')
        ];
        $filterParams = $this->searchables($request) + $filterParams;
        return $this->employeeRepo->getEmployees($filterParams);
    }

    public function searchables(Request $request): array
    {
        return [
            'name' => $request->get('name'),
            'employee_number' => $request->get('employee_number'),
            'email' => $request->get('email'),
            'contact' => $request->get('contact'),
            'designation' => $request->get('designation'),
            'records_per_page' => $request->has('records_per_page')
                ? $request->get('records_per_page') : 10,
            'sorting' => $request->has('sorting')
                ? $request->get('sorting') : 'desc'
        ];
    }

    public function saveEmployee($validatedRequests)
    {
        try {
            DB::beginTransaction();
            $employee = $this->employeeRepo->saveEmployee($validatedRequests);
            $this->employeeRepo->syncEmployeeInDepartments($employee, $validatedRequests['department_ids']);
            DB::commit();
            return $employee;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getEmployeesOfCompany(Request $request, $companyId)
    {
        try {
            $filterParams = [
                'company_id' => $companyId,
                'department_ids' => $request->get('department_ids')
            ];
            $filterParams = $this->searchables($request) + $filterParams;
            $employees = $this->employeeRepo->getEmployees($filterParams);
            return $employees;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getEmployeesOfDepartmentOfCompany(Request $request, $companyId, $departmentId)
    {
        try {
            $filterParams = [
                'company_id' => $companyId,
                'department_ids' => [$departmentId]
            ];
            $filterParams = $this->searchables($request) + $filterParams;
            $employees = $this->employeeRepo->getEmployees($filterParams);
            return $employees;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function showEmployeeDetails($employeeId)
    {
        try {
            $employee = $this->employeeRepo->showEmployeeDetails($employeeId);
            if (!$employee) {
                throw new Exception('No Such Employee Found', 404);
            }
            return $employee;
        } catch (Exception $e) {
            throw $e;
        }
    }

}
