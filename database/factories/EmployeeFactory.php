<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory
 */
class EmployeeFactory extends Factory
{

    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $user = User::factory()->create();
        return [
            'name' => fake()->name,
            'email' => fake()->safeEmail,
            'contact' => fake()->numerify('##-######'),
            'designation' => Str::random(10),
            'created_by' => $user->id
        ];
    }
}
