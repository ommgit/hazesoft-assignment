<?php

use App\Http\Controllers\Api\Admin\AdminAuthController;
use App\Http\Controllers\Api\Company\CompanyController;
use App\Http\Controllers\Api\Department\DepartmentController;
use App\Http\Controllers\Api\Employee\EmployeeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//admin login
Route::controller(AdminAuthController::class)->prefix('admin')->group(function () {
    Route::post('/login', 'login');
    Route::post('/logout', 'logout')->middleware('auth:api');
});

//company
Route::controller(CompanyController::class)
    ->group(function () {
        Route::get('companies', 'index');
        Route::post('companies', 'store')->middleware('auth:api');
    });


//department
Route::controller(DepartmentController::class)
    ->group(function () {
        Route::middleware('auth:api')->group(function () {
            Route::get('departments', 'index');
            Route::get('companies/{company_id}/departments', 'getDepartmentsOfCompany');
            Route::post('departments', 'store');
        });
    });

//employee
Route::controller(EmployeeController::class)
    ->group(function () {
        Route::middleware('auth:api')->group(function () {
            Route::get('employees', 'index');
            Route::post('employees', 'store');
            Route::get(
                'companies/{company_id}/departments/{department_id}/employees',
                'getEmployeesOfDepartmentOfCompany'
            );
            Route::get('employees/{employee_id}', 'showEmployeeDetails');
        });
        Route::get('companies/{company_id}/employees', 'getEmployeesOfCompany');
    });