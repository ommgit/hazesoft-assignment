<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    public function testMustEnterEmailAndPasswordForLogin()
    {
        $this->json('POST', 'api/admin/login', [], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ])
            ->assertStatus(422);
    }

    public function testSuccessfulLogin()
    {
        $plainPassword = 'secret123';
        $user = User::factory()->create([
            'password' => bcrypt($plainPassword),
            'is_admin' => 1
        ]);
        $payload = ['email' => $user->email, 'password' => $plainPassword];

        $this->json('POST', 'api/admin/login', $payload, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "error",
                "message",
                "code",
                "data" => [
                    "user" => [
                        "name",
                        "email",
                        "is_admin"
                    ],
                    "access_token"
                ]
            ]);
    }

    public function testInvalidCredentialsWhileLogin()
    {
        $userData = [
            "email" => "doe@example.com",
            "password" => "admin123",
        ];

        $this->json('POST', 'api/admin/login', $userData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure([
                "error",
                "message",
                "code",
            ]);
    }
}
