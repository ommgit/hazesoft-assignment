<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class CompanyControllerTest extends TestCase
{

    public function testMustBeAuthenticatedToCreateCompany()
    {
        $companyData = [
            'name' => Str::random(16),
            'location' => Str::random(40),
            'contact' => 9808590013
        ];
        $this->json('POST', 'api/companies', $companyData, ['Accept' => 'application/json'])
            ->assertStatus(401);
    }

    public function testCompanyCreatedSuccessfully()
    {
        $companyData = [
            'name' => Str::random(16),
            'location' => Str::random(40),
            'contact' => 9808590013
        ];
        $user = User::factory()->create([
            'is_admin' => 1
        ]);
        $this->actingAs($user, 'api');

        $this->json('POST', 'api/companies', $companyData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    'id',
                    'name',
                    'location',
                    'contact',
                    'created_at',
                ]
            ]);
    }

    public function testCompaniesListedSuccessfully()
    {
        $this->json('GET', 'api/companies', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
}
