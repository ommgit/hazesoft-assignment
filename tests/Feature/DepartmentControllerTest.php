<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\Department;
use App\Models\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class DepartmentControllerTest extends TestCase
{
    public function testMustBeAuthenticatedToCreateDepartment()
    {
        $user = User::factory()->create();
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        $departmentData = [
            'name' => Str::random(16),
            'company_id' => $company->id
        ];
        $this->json('POST', 'api/companies', $departmentData, ['Accept' => 'application/json'])
            ->assertStatus(401);
    }

    public function testDepartmentCreatedSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        $departmentData = [
            'name' => Str::random(16),
            'company_id' => $company->id
        ];

        $this->json('POST', 'api/departments', $departmentData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    'id',
                    'name',
                    'company',
                    'created_at',
                ]
            ]);
    }

    public function testDepartmentsListingOfACompany()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        Department::factory()->create([
            'name' => "Logistics",
            'company_id' => $company->id
        ]);
        Department::factory()->create([
            'name' => "Operations",
            'company_id' => $company->id
        ]);

        $this->json('GET', 'api/companies/' . $company->id . '/departments', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

}
