<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\Department;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    public function testMustBeAuthenticatedToAddEmployee()
    {
        $user = User::factory()->create();
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        $departments = Department::factory()->count(2)->create([
            'company_id' => $company->id,
            'created_by' => $user->id
        ]);
        $departmentIds = $departments->pluck('id')->toArray();
        $employeeData = [
            'name' => Str::random(16),
            'email' => Str::random(10) . '@gmail.com',
            'contact' => random_int(10, 15),
            'designation' => Str::random(15),
            'company_id' => $company->id,
            'department_ids' => $departmentIds
        ];
        $this->json('POST', 'api/employees', $employeeData, ['Accept' => 'application/json'])
            ->assertStatus(401);
    }

    public function testEmployeeCreatedSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        $departments = Department::factory()->count(2)->create([
            'company_id' => $company->id,
            'created_by' => $user->id
        ]);
        $departmentIds = $departments->pluck('id')->toArray();
        $employeeData = [
            'name' => Str::random(16),
            'email' => Str::random(10) . '@gmail.com',
            'contact' => random_int(10, 15),
            'designation' => Str::random(15),
            'company_id' => $company->id,
            'department_ids' => $departmentIds
        ];
        $this->json('POST', 'api/employees', $employeeData, ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

    public function testFetchingEmployeeDetails()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        $departments = Department::factory()->count(2)->create([
            'company_id' => $company->id,
            'created_by' => $user->id
        ]);
        $departmentIds = $departments->pluck('id')->toArray();
        $employee = Employee::factory()->create([
            'company_id' => $company->id
        ]);
        $employee->departments()->attach($departmentIds);

        $this->json('GET', 'api/employees/' . $employee->id . '', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

    public function testEmployeesListingOfACompany()
    {
        $user = User::factory()->create();
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        $departments = Department::factory()->count(2)->create([
            'company_id' => $company->id,
            'created_by' => $user->id
        ]);
        $departmentIds = $departments->pluck('id')->toArray();
        $employees = Employee::factory()->count(4)->create([
            'company_id' => $company->id
        ]);
        $employees->map(function ($employee) use ($departmentIds) {
            $employee->departments()->attach($departmentIds);
        });

        $this->json(
            'GET',
            'api/companies/' . $company->id . '/employees',
            ['name' => 'Shramik'],
            ['Accept' => 'application/json']
        )
            ->assertStatus(200);
    }

    public function testEmployeesListingOfADepartmentInACompany()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $company = Company::factory()->create([
            'created_by' => $user->id
        ]);
        $department1 = Department::factory()->create([
            'company_id' => $company->id,
            'created_by' => $user->id
        ]);
        $department2 = Department::factory()->create([
            'company_id' => $company->id,
            'created_by' => $user->id
        ]);

        $employeesSet1 = Employee::factory()->count(4)->create([
            'name' => Str::random(20) . ' Set 1',
            'company_id' => $company->id,
        ]);
        $employeesSet1->map(function ($employeeSet1) use ($department1, $department2) {
            $employeeSet1->departments()->attach([$department1->id, $department2->id]);
        });
        $employeesSet2 = Employee::factory()->count(4)->create([
            'name' => Str::random(12) . ' Set 2',
            'company_id' => $company->id,
        ]);
        $employeesSet2->map(function ($employeeSet2) use ($department2) {
            $employeeSet2->departments()->attach([$department2->id]);
        });

        $this->json(
            'GET',
            'api/companies/' . $company->id . '/departments/' . $department1->id . '/employees',
            ['name' => 'Set 1'],
            ['Accept' => 'application/json']
        )
            ->assertStatus(200);
    }


}
